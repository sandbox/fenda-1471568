<?php

/**
 * @file
 * Views interface for Entity wall module.
 */

/**
 * Implements hook_views_data().
 */
function entity_wall_views_data() {
  $data['entity_wall_statistics'] = array(
    'table' => array(
      'group' => t('Entity wall'),
      'join' => array(
        'node' => array(
          'left_field' => 'nid',
          'field' => 'entity_id',
          'extra' => array(
            array('field' => 'entity_type', 'value' => 'node'),
          ),
        ),
      ),
    ),
    'last_message_timestamp' => array(
      'title' => t('Last message time'),
      'help' => t('Date and time of when the last message was posted.'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
    ),
    'last_message_uid' => array(
      'title' => t('Last message uid'),
      'help' => t('The User ID of the author of the last message of a node wall.'),
      'relationship' => array(
        'title' => t('Last message author'),
        'base' => 'users',
        'base field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('Last message author'),
      ),
    ),
    'message_count' => array(
      'title' => t('Message count'),
      'help' => t('The number of messages a node has.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    )
  );

  return $data;
}
