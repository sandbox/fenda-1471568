<?php

/**
 * @file
 * Admin page callback file for the entity_wall module.
 */

/**
 * Form builder; Configure entity wall settings for this site.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function entity_wall_admin_settings() {
  $form = array();

  $form['entity_wall_limit'] = array(
    '#type' => 'select',
    '#title' => t('Number of messages'),
    '#description' => t('The number of messages displayed initially on walls.'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30)),
    '#default_value' => variable_get('entity_wall_limit', 10),
  );
  $form['entity_wall_comment_limit'] = array(
    '#type' => 'select',
    '#title' => t('Number of comments'),
    '#description' => t('The number of comments displayed initially beneath messages.'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30)),
    '#default_value' => variable_get('entity_wall_comment_limit', 2),
  );
  $form['entity_wall_today_time'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display time only for messages posted within the last 24-hours'),
    '#default_value' => variable_get('entity_wall_today_time', 1),
  );

  $info = entity_get_info();

  // Create an array of entity type keys
  $options = array();
  foreach ($info as $type => $entity_type) {
    $options[$type] = $entity_type['label'];
  }

  // Don't allow a wall on a wall, that would just be silly. :)
  unset($options['entity_wall_message']);

  $form['entity_wall_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#options' => $options,
    '#default_value' => variable_get('entity_wall_types', array()),
    '#description' => t('A message wall will be added automatically to entity types selected here. You can control settings for individual node types <a href="@url">here</a>.', array('@url' => url('admin/structure/types'))),
  );

  return system_settings_form($form);
}
