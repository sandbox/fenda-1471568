<?php

/**
 * @file
 * Page callback file for the entity_wall module.
 */

/**
 * Menu callback; view a single entity wall message.
 */
function entity_wall_message_page_view($message) {
  $build = array(
    '#prefix' => '<div id="entity-wall">',
    '#suffix' => '</div>',
    '#weight' => 30,
    '#attached' => array(
      'library' => array(
        array('entity_wall', 'entity-wall'),
      ),
    ),
  );

  // Set up a wall of a the message and it's comments
  $messages = entity_wall_message_load_comments($message->id);
  $messages[$message->id] = $message;
  $build_messages = entity_wall_message_view_multiple($messages, 'full');
  $build['message'] = entity_wall_organize_comments($build_messages);

  return $build;
}

/**
 * Menu callback; Delete an entity wall message
 */
function entity_wall_message_delete_callback($message, $type) {
  entity_wall_message_delete($message->id);

  if ($type == 'ajax') {
    $commands = array();
    $commands[] = ajax_command_remove('#message-' . $message->id); // TODO slide up + remove

    if (empty($message->pid)) {
      // Update the hidden offset form value
      $commands[] = ajax_command_invoke(NULL, 'entityWallUpdateOffset', array(-1));
    }

    $build = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );

    ajax_deliver($build);
  }
  else {
    drupal_set_message(t('Message deleted succesfully.'));
    drupal_goto();
  }
}

/**
 * Menu callback; Return comments for a given message
 */
function entity_wall_message_comments($message, $type) {
  if ($type == 'ajax') {
    $commands = array();
    $commands[] = ajax_command_remove('#message-' . $message->id . ' .message-comments-more');

    if ($messages = entity_wall_message_load_comments($message->id)) {
      $build = entity_wall_message_view_multiple($messages, 'full');
      $commands[] = ajax_command_html('#message-' . $message->id . ' .message-comments', render($build));
    }

    $build = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );

    ajax_deliver($build);
  }
  else {
    // NOJS requests are theoretically not supported, but we allow them
    // to get this far to prevent menu callback errors.
    drupal_goto();
  }
}
