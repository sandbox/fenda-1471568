(function($) {
  /**
   * Adjust the hidden offset form value for entity wall more buttons
   */
  $.fn.entityWallUpdateOffset = function(change) {
    // Retrieve the current offset value
    var current = $('#entity-wall-more-form input:hidden[name=offset]').val();
    // Ensure it is an integer
    if (Drupal.checkPlain(current)) {
      // Perform math with the given offset
      var offset = parseInt(current) + change;
      // Set the new value
      $('#entity-wall-more-form input:hidden[name=offset]').val(offset);
    }
  };
})(jQuery);
