<?php

/**
 * @file
 * Default theme implementation to display a entity wall message
 *
 * Available variables:
 * - $team_url: Direct url of the current message.
 *
 * There may be more available variables, see template_preprocess_entity_wall_message() for details.
 *
 * @see template_preprocess_entity_wall_message()
 */
?>
<div id="message-<?php print $message->id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if (isset($picture)): ?>
    <div class="message-picture">
      <?php print $picture; ?>
    </div>
  <?php endif; ?>

  <div class="message-content-wrapper">
    <?php if (isset($title)): ?>
      <?php print render($title_prefix); ?>
      <div class="message-title">
        <?php print $title; ?>
      </div>
      <?php print render($title_suffix); ?>
    <?php endif; ?>


     <?php if (isset($links)): ?>
      <div class="message-links">
         <?php print render($links); ?>
      </div>
    <?php endif; ?>

    <div class="message-content">
      <?php print render($content); ?>
    </div>

    <div class="message-date">
      <?php print $date; ?>
    </div>
  </div>

  <?php if ($parent && $view_mode != 'flat'): ?>
    <div class="message-comments-wrapper">
      <div class="message-comments">
      <?php if (isset($comments)): ?>
          <?php print render($comments); ?>
      <?php endif; ?>
      </div>

    <?php if (isset($message_form)): ?>
      <div class="message-form">
        <?php print render($message_form); ?>
      </div>
    <?php endif; ?>
    </div>
  <?php endif; ?>
</div>
