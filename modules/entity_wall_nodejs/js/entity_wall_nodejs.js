
(function ($) {
  Drupal.Nodejs.callbacks.entityWallMessageInsert = {
    callback: function (message) {
      if ($('#entity-wall.single-entity-wall').length > 0) {
        if ($('.page-' + message.data.entity_type + '-' + message.data.entity_id + ' #entity-wall').length === 0) {
          // If we are viewing a single entity wall then we are only care about messages
          // directly related to this entity.
          return;
        }
      }

      // Target the refresh forms submit button
      var submit = "#entity-wall-refresh-form input[type=submit]";

      // Show the button
      $(submit).removeClass('element-invisible');

      // The current update count is stored as an attribute
      var count = $(submit).attr('data-item-count');

      // Increase the update count
      count = parseInt(count) + 1;
      $(submit).attr('data-item-count', count);

      var string = Drupal.t('@count new updates', { '@count': count });
      $(submit).attr('value', string);
    }
  };

}(jQuery));